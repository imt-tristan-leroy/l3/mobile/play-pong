import 'package:flutter/material.dart';
import 'package:play_pong/pages/platform.dart';

import 'ball.dart';

enum Direction { top, bottom, left, right }

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePage();
}

class _HomePage extends State<HomePage> with SingleTickerProviderStateMixin {
  late Animation animation;
  late AnimationController controller;

  Direction vDir = Direction.bottom;
  Direction hDir = Direction.left;

  double increment = 10;

  int score = 0;

  double width = 400;
  double height = 400;
  double posX = 0;
  double posY = 0;
  double widthPlatform = 0;
  double heightPlatform = 0;
  double positionPlatform = 0;

  void collisionTest() {
    if (posX < Ball.diameter) {
      hDir = Direction.right;
    }
    if (posX > width - Ball.diameter) {
      hDir = Direction.left;
    }
    if (posY < Ball.diameter) {
      vDir = Direction.bottom;
    }
    if (posY > height - heightPlatform - Ball.diameter) {
      if (posX + Ball.diameter > positionPlatform &&
          posX < positionPlatform + widthPlatform) {
        score++;
        vDir = Direction.top;
      } else {
        controller.stop();
        displayMessage(context);
        score = 0;
      }
    }
  }

  void movePlatform(DragUpdateDetails maj, BuildContext context) {
    safeSetState(() {
      positionPlatform += maj.delta.dx;
    });
  }

  void safeSetState(Function function) {
    if (mounted && controller.isAnimating) {
      setState(() {
        function();
      });
    }
  }

  void displayMessage(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("GAME OVER"),
          content:
              Text('Your score: $score Do you want to start another game ?'),
          actions: [
            FlatButton(
              textColor: Colors.blue,
              onPressed: () => {
                setState(() {
                  posY = 0;
                  posX = 0;
                }),
                Navigator.pop(context),
                controller.repeat()
              },
              child: const Text('YES'),
            ),
            FlatButton(
              textColor: Colors.blue,
              onPressed: () => {Navigator.pop(context), dispose()},
              child: const Text('NO'),
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    posX = 0;
    posY = 0;
    controller = AnimationController(
      duration: const Duration(seconds: 10000),
      vsync: this,
    );
    animation = Tween<double>(begin: 0, end: 100).animate(controller);
    animation.addListener(() {
      safeSetState(() {
        (hDir == Direction.right) ? posX += increment : posX -= increment;
        (vDir == Direction.bottom) ? posY += increment : posY -= increment;
      });
      collisionTest();
    });
    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          height = constraints.maxHeight;
          width = constraints.maxWidth;
          widthPlatform = width / 5;
          heightPlatform = height / 20;
          return Stack(
            children: <Widget>[
              Positioned(top: 10, left: 300, child: Text("Score : $score")),
              Positioned(
                top: posY,
                left: posX,
                child: const Ball(),
              ),
              Positioned(
                bottom: 0,
                left: positionPlatform,
                child: GestureDetector(
                  onHorizontalDragUpdate: (DragUpdateDetails maj) =>
                      movePlatform(maj, context),
                  child: Platform(
                    width: widthPlatform,
                    height: heightPlatform,
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
