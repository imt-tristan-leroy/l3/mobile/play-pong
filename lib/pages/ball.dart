import 'package:flutter/material.dart';

class Ball extends StatelessWidget {
  const Ball({Key? key}) : super(key: key);
  static double diameter = 50;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: diameter,
      height: diameter,
      decoration:
          const BoxDecoration(shape: BoxShape.circle, color: Colors.yellow),
    );
  }
}
