import 'package:flutter/material.dart';

class Platform extends StatelessWidget {
  const Platform({Key? key, required this.height, required this.width})
      : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: const BoxDecoration(color: Colors.blue),
    );
  }
}
